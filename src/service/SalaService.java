package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Sala;
import model.SalaDAO;



@Service
public class SalaService {
	private SalaDAO messages;

	public SalaDAO getSala() {
		return messages;
	}

	@Autowired
	public void setSala(SalaDAO messages) {
		this.messages = messages;
	}

	public List<Sala> getCurrent() {
		return messages.getSala();
	}

	public boolean create(Sala cinema) {
		return messages.create(cinema);
	}

	public boolean delete(String id) {
		return messages.delete(id);
	}
}
