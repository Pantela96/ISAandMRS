package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Cinema;
import model.CinemasDAO;


@Service
public class CinemaService {
	private CinemasDAO cinemas;

	public CinemasDAO getUsers() {
		return cinemas;
	}
	
	@Autowired
	public void setUsers(CinemasDAO movies) {
		this.cinemas = movies;
	}
	
	public List<Cinema> getCurrent(){
		return cinemas.getCinemas();
	}
	
	public boolean create(Cinema cinema) {
		return cinemas.create(cinema);
	}
}
