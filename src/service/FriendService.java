package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Friend;
import model.FriendsDAO;


@Service
public class FriendService {
	
	private FriendsDAO friends;

	public FriendsDAO getFriends() {
		return friends;
	}
	
	@Autowired
	public void setFriends(FriendsDAO friends) {
		this.friends = friends;
	}
	
	public List<Friend> getCurrent(){
		return friends.getFriends();
	}
	
	public boolean create(Friend friend) {
		return friends.create(friend);
	}
	public boolean delete(String id) {
		System.out.println(id + "ma ovo je");
		return friends.delete(id);
	}

}
