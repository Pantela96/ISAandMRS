package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Message;
import model.MessagesDAO;
import model.User;



@Service
public class MessageService {
	private MessagesDAO messages;

	public MessagesDAO getMessages() {
		return messages;
	}

	@Autowired
	public void setMessages(MessagesDAO messages) {
		this.messages = messages;
	}

	public List<Message> getCurrent() {
		return messages.getMessages();
	}

	public boolean create(Message cinema) {
		return messages.create(cinema);
	}
	
	public boolean delete(String id) {
		return messages.delete(id);
	}
}
