package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Movie;
import model.MoviesDAO;

@Service
public class MovieService {
	private MoviesDAO movies;

	public MoviesDAO getUsers() {
		return movies;
	}
	
	@Autowired
	public void setUsers(MoviesDAO movies) {
		this.movies = movies;
	}
	
	public List<Movie> getCurrent(){
		return movies.getMovies();
	}
	
	public boolean create(Movie movie) {
		return movies.create(movie);
	}
}
