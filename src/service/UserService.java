package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.User;
import model.UsersDAO;

@Service
public class UserService {
	private UsersDAO users;

	public UsersDAO getUsers() {
		return users;
	}
	
	@Autowired
	public void setUsers(UsersDAO users) {
		this.users = users;
	}
	
	public Boolean updateUser(User offer){
		return users.update(offer);
	}
	
	public List<User> getCurrent(){
		return users.getUsers();
	}
	
	public boolean create(User user) {
		return users.create(user);
	}
	
}
