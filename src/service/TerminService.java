package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Termin;
import model.TerminDAO;


@Service
public class TerminService {
	private TerminDAO termin;

	public TerminDAO getTheaters() {
		return termin;
	}
	
	@Autowired
	public void setTheaters(TerminDAO termin) {
		this.termin = termin;
	}
	
	public List<Termin> getCurrent(){
		return termin.getTermin();
	}
	
	public boolean create(Termin termin1) {
		return termin.create(termin1);
	}
}
