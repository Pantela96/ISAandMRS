package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Friend;
import model.FriendsDAO;
import model.Theater;
import model.TheaterDAO;
@Service
public class TheaterService {
	private TheaterDAO friends;

	public TheaterDAO getTheaters() {
		return friends;
	}
	
	@Autowired
	public void setTheaters(TheaterDAO friends) {
		this.friends = friends;
	}
	
	public List<Theater> getCurrent(){
		return friends.getTheaters();
	}
	
	public boolean create(Theater friend) {
		return friends.create(friend);
	}
}
