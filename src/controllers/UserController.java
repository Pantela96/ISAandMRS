package controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import model.Cinema;
import model.Friend;
import model.Message;
import model.Movie;
import model.Sala;
import model.Sediste;
import model.Termin;
import model.Theater;
import model.User;
import service.CinemaService;
import service.FriendService;
import service.MessageService;
import service.MovieService;
import service.SalaService;
import service.TerminService;
import service.TheaterService;
import service.UserService;

@Controller

public class UserController {

	private UserService userService;
	private MovieService movieService;
	private MessageService messageService;
	private FriendService friendService;
	private TheaterService cinemaService;
	private TerminService terminService;
	private SalaService salaService;
	private String code;
	private Cinema currentCinema;
	private CinemaService bioskopi;

	public CinemaService getCinemaService() {
		return bioskopi;
	}

	@Autowired
	public void setCinemaService(CinemaService cinemaService) {
		this.bioskopi = cinemaService;
	}

	public SalaService getSalaService() {
		return salaService;
	}

	@Autowired
	public void setSalaService(SalaService salaService) {
		this.salaService = salaService;
	}

	public TerminService getTerminService() {
		return terminService;
	}

	@Autowired
	public void setTerminService(TerminService terminService) {
		this.terminService = terminService;
	}

	public TheaterService getTheaterService() {
		return cinemaService;
	}

	@Autowired
	public void setTheaterService(TheaterService cinemaService) {
		this.cinemaService = cinemaService;
	}

	public FriendService getFriendService() {
		return friendService;
	}

	@Autowired
	public void setFriendService(FriendService friendService) {
		this.friendService = friendService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public MovieService getMovieService() {
		return movieService;
	}

	@Autowired
	public void setMovieService(MovieService movieService) {
		this.movieService = movieService;
	}

	public UserService getCityService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping("/")
	public String showHome(Model model, HttpSession session) {
		if (session.getAttribute("user") == null) {
			User novi = new User("x", "x", "x", "x", "x", "false", "default-avatar.png");
			session.setAttribute("user", novi);
			List<User> users = userService.getCurrent();
			session.setAttribute("users", users);
			session.setAttribute("userService", userService);
			return "home";
		} else {

			return "home";
		}

	}

	@RequestMapping("/logoutHandle")
	public String showLogout(HttpSession session, Model model) {
		session.removeAttribute("user");
		session.setAttribute("user", null);
		System.out.println("ovde sam");

		showHome(model, session);
		return "home";
	}

	@RequestMapping("/check")
	public String showCheck(Model model, @RequestParam("uname") String user, @RequestParam("psw") String password,
			HttpSession session) {
		List<User> users = userService.getCurrent();
		List<Message> messages = messageService.getCurrent();
		List<Friend> friends = friendService.getCurrent();

		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		User u = getUserByUsername(user);
		session.setAttribute("user", u);
		if (session.getAttribute("user") == null) {
			User novi = new User("x", "x", "x", "x", "x", "false", "");
			session.setAttribute("user", novi);
			return "home";
		}
		for (Friend f : friends) {

			if (f.getOrigin().equalsIgnoreCase(u.getId())) {

				u.getFriendList().add(getUserByID(f.getOther(), users));
			}
		}

		for (Message m : messages) {
			if (m.getSender().equalsIgnoreCase(u.getId())) {
				u.getSendedRequests().add(getUserByID(m.getReceiver(), users));
				count1 = u.getSendedRequests().size();
			} else if (m.getReceiver().equalsIgnoreCase(u.getId())) {
				u.getReceivedRequests().add(getUserByID(m.getSender(), users));
				count2 = u.getReceivedRequests().size();
			}
		}

		System.out.println(u.getPassword() + " u.getId() " + password + " userID");
		if (u.getId().equalsIgnoreCase(getIDByUsername(user)) && u.getPassword().equalsIgnoreCase(password)) {
			session.setAttribute("user", u);
			count3 = u.getFriendList().size();
			session.setAttribute("counter1", count1);
			session.setAttribute("counter2", count2);
			session.setAttribute("counter3", count3);

			return "home";
		} else {
			User novi = new User("x", "x", "x", "x", "x", "false", "default-avatar.png");
			session.setAttribute("user", novi);
			return "home";
		}

	}

	public User getUserByID(String username, List<User> users) {
		for (User u : users) {
			if (u.getId().equalsIgnoreCase(username)) {
				return u;
			}
		}
		return null;
	}

	public User getUserByUsername(String id) {
		List<User> users = userService.getCurrent();
		System.out.println(users + "useri");
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(id)) {
				System.out.println("usao");
				return u;
			}
		}
		return null;
	}

	public String getIDByUsername(String username) {
		String id = "";
		List<User> users = userService.getCurrent();
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(username)) {
				return u.getId();
			}
		}
		return id;
	}

	@RequestMapping("/register")
	public String showRegister(HttpSession session, Model model, @RequestParam("uname") String username,
			@RequestParam("psw") String password, @RequestParam("email") String email,
			@RequestParam("type") String type, @RequestParam("psw1") String password1, HttpServletRequest request)
			throws IOException, ServletException {
		List<User> users = userService.getCurrent();
		Boolean flag = false;

		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(username) || !(password.equalsIgnoreCase(password1))
					|| u.getEmail().equalsIgnoreCase(email)) {
				flag = true;
			}
		}

		if (flag) {
			model.addAttribute("isGood", false);
			return "home";
		} else {
			code = sendEmail(email);
			User user = new User(code, username, password, email, type, "false", "default-avatar.png");
			session.setAttribute("user", user);
			model.addAttribute("isGood", true);
			userService.create(user);
			return "home";
		}
	}

	@RequestMapping("requestdecline")
	public String deleteRequests(Model model, HttpSession session, @RequestParam("username") String username) {
		User user = (User) session.getAttribute("user");
		for (java.util.Iterator<User> iterator = user.getReceivedRequests().iterator(); iterator.hasNext();) {
			User s = iterator.next();
			if (s.getUsername().equalsIgnoreCase(username)) {
				iterator.remove();
			}
		}
		session.removeAttribute("counter2");
		session.setAttribute("counter2", user.getReceivedRequests().size());
		return "home";
	}

	@RequestMapping("requestshandle")
	public String showRequests(Model model, HttpSession session, @RequestParam("username") String username) {

		User user = (User) session.getAttribute("user");
		List<User> users = userService.getCurrent();
		List<Friend> friends = friendService.getCurrent();
		int inc = 1;
		List<Message> messages = messageService.getCurrent();
		Friend friend = new Friend(String.valueOf(Integer.valueOf(friends.get(friends.size() - 1).getId()) + inc),
				getIDByUsername(user.getUsername()), getIDByUsername(username));
		Friend friend2 = new Friend(String.valueOf(Integer.valueOf(friends.get(friends.size() - 1).getId()) + inc + 1),
				getIDByUsername(username), getIDByUsername(user.getUsername()));
		Boolean check = false;
		for (User f : user.getFriendList()) {
			if (f.getUsername().equalsIgnoreCase(username)) {
				check = true;
			}
		}

		if (check == false) {
			user.getFriendList().add(getUserByUsername(username));
			getUserByUsername(username).getFriendList().add(user);
			friendService.create(friend);
			friendService.create(friend2);
		}

		for (java.util.Iterator<User> iterator = user.getReceivedRequests().iterator(); iterator.hasNext();) {
			User s = iterator.next();
			if (s.getUsername().equalsIgnoreCase(username)) {
				iterator.remove();
			}
		}
		String id = getIDByUsername(username);
		for (Message m : messages) {
			if (m.getReceiver().equalsIgnoreCase(user.getId())) {
				if (m.getSender().equalsIgnoreCase(id)) {
					messageService.delete(m.getId());
				}
			}
		}
		session.removeAttribute("counter3");
		session.setAttribute("counter3", user.getFriendList().size());
		session.removeAttribute("counter2");
		session.setAttribute("counter2", user.getReceivedRequests().size());

		return "home";
	}

	@RequestMapping("deletehandle")
	public String showDelete(Model model, HttpSession session, @RequestParam("username") String username) {
		User user = (User) session.getAttribute("user");
		List<Friend> friends = friendService.getCurrent();
		System.out.println(friends.size());
		int counter = 0;
		for (java.util.Iterator<User> iterator = user.getFriendList().iterator(); iterator.hasNext();) {
			User s = iterator.next();
			if (s.getUsername().equalsIgnoreCase(username)) {
				iterator.remove();
				for (Iterator<Friend> iterator2 = friends.iterator(); iterator2.hasNext();) {
					Friend f = iterator2.next();
					System.out.println(f.getOrigin() + " " + getIDByUsername(username) + "  hejeeh");
					if (f.getOrigin().equalsIgnoreCase(getIDByUsername(username))
							|| f.getOther().equalsIgnoreCase(getIDByUsername(username))) {
						if (counter < 3) {
							iterator2.remove();
							counter++;
							friendService.delete(f.getId());
							session.removeAttribute("counter3");
							session.setAttribute("counter3", user.getFriendList().size());
							session.removeAttribute("user");
							session.setAttribute("user", user);
						} else {
							return "home";
						}

					}

				}
			}
		}
		session.removeAttribute("counter3");
		session.setAttribute("counter3", user.getFriendList().size());
		session.removeAttribute("user");
		session.setAttribute("user", user);
		return "home";
	}

	@RequestMapping("addFriend")
	public String showFriend(Model model, HttpSession session, @RequestParam("uname") String username) {
		List<User> users = userService.getCurrent();
		User user = (User) session.getAttribute("user");
		List<Message> messages = messageService.getCurrent();

		int inc = 1;
		Boolean flag = false;
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(username) && !(username.equalsIgnoreCase(user.getUsername()))
					&& !(checkExist(user, username))) {
				flag = true;
				if (messages.size() == 0) {
					messageService
							.create(new Message(getIDByUsername(user.getUsername()), getIDByUsername(username), "1"));
				} else {
					Message m = new Message(getIDByUsername(user.getUsername()), getIDByUsername(username),
							String.valueOf(Integer.valueOf(messages.get(messages.size() - 1).getId()) + inc));
					messages.add(m);
					messageService.create(m);
				}

			}
		}

		if (flag == false) {
			model.addAttribute("isGood1", flag);
		} else {
			model.addAttribute("isGood1", flag);
		}
		System.out.println(user.getFriendList().size());
		session.removeAttribute("counter3");
		session.setAttribute("counter3", user.getFriendList().size());
		return "home";
	}

	private boolean checkExist(User user, String username) {
		Boolean check = false;
		for (User u : user.getFriendList()) {
			if (u.getUsername().equalsIgnoreCase(username)) {
				check = true;
			}
		}
		return check;
	}

	private String sendEmail(String email) {

		// Sender's email ID needs to be mentioned
		String from = "awesomedude996@gmail.com";
		String pass = "pantelaa23";
		// Recipient's email ID needs to be mentioned.
		String to = "milospantic96@hotmail.com";
		String code = generateCode();
		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();
		// Setup mail server
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.user", from);
		properties.put("mail.smtp.password", pass);
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(RecipientType.TO, new InternetAddress(to));

			message.setSubject("This is activation code. Use it to activate your account.");

			message.setText(code);

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("Sent message successfully....");
			return code;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return "";
	}

	@RequestMapping("/activate")
	public String activateShow(Model model, HttpSession session, @RequestParam("code") String active) {
		List<User> users = userService.getCurrent();
		Boolean isGood2 = false;

		for (User a : users) {
			if (a.getId().equalsIgnoreCase(active)) {
				a.setActive("true");
				isGood2 = true;
				session.removeAttribute("user");
				session.setAttribute("user", a);
				userService.updateUser(a);
			}
		}

		model.addAttribute("isGood2", isGood2);
		return "home";

	}

	@RequestMapping("/changeProfile")
	public String changeProfile(Model model, HttpSession session, @RequestParam("uname") String username,
			@RequestParam("email") String email, @RequestParam("type") String type) {
		List<User> users = userService.getCurrent();
		boolean flag = false;
		boolean flag2 = false;
		User user = (User) session.getAttribute("user");
		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(user.getUsername())) {
				for (User us : users) {
					if (us.getUsername().equalsIgnoreCase(username)) {
						flag = true;
						model.addAttribute("flagchange", flag);
						return "home";
					}
					if (us.getEmail().equalsIgnoreCase(email)) {
						flag2 = true;
						model.addAttribute("flagchange2", flag2);
						return "home";
					}
				}
				model.addAttribute("flagchange", flag);
				u.setUsername(username);
				u.setEmail(email);
				u.setTip(type);
				session.removeAttribute("user");
				session.setAttribute("user", u);
				userService.updateUser(u);

			}
		}

		return "home";

	}

	@RequestMapping("/changePassword")
	public String changePassword(Model model, HttpSession session, @RequestParam("psw") String password,
			@RequestParam("psw1") String password1, @RequestParam("psw2") String password2) {
		List<User> users = userService.getCurrent();
		int flag = 0;
		int flag2 = 0;
		User user = (User) session.getAttribute("user");

		if (user.getPassword().equalsIgnoreCase(password)) {

			if (password.equalsIgnoreCase(password1)) {
				user.setPassword(password2);
				session.removeAttribute("user");
				session.setAttribute("user", user);
				userService.updateUser(user);
				model.addAttribute("flagpassword", flag2);
				return "home";
			} else {
				flag2 = 1;

			}
		} else {
			flag = 1;
		}

		model.addAttribute("flagpassword2", flag2);
		model.addAttribute("flagpassword", flag);
		return "home";

	}

	private String generateCode() {
		RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder().withinRange('0', 'z')
				.filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS).build();
		String s = randomStringGenerator.generate(12);
		return s;
	}

	@RequestMapping("/pozorista")
	public String showCinemas(Model model) {
		List<Theater> cinemas = cinemaService.getCurrent();
		model.addAttribute("theaters", cinemas);
		return "pozorista";
	}

	@RequestMapping("/changePhoto")
	public String changePhoto(Model model, HttpSession session) throws IOException, ServletException {

		return "home";
	}

	@RequestMapping("/list")
	public String getList(Model model, HttpSession session) throws IOException, ServletException {
		List<Theater> cinemas = cinemaService.getCurrent();
		model.addAttribute("cinemas", cinemas);
		List<Cinema> cinemas2 = bioskopi.getCurrent();
		model.addAttribute("cinemas", cinemas2);
		model.addAttribute("theaters", cinemas);
		return "list";
	}

	@RequestMapping("/rezervacija")
	public String reservation(Model model, HttpSession session, @RequestParam("ustanova") String ustanova)
			throws IOException, ServletException, ParseException {
		List<Cinema> cinemas = bioskopi.getCurrent();
		List<Movie> movies = movieService.getCurrent();
		List<Movie> movies2 = new ArrayList<Movie>();
		for (Cinema c : cinemas) {
			if (c.getId().equalsIgnoreCase(ustanova)) {
				currentCinema = c;
			}
			if (c.getId().equalsIgnoreCase(ustanova)) {
				for (String m : c.getMovies()) {
					for (Movie mov : movies) {
						if (m.equalsIgnoreCase(mov.getId())) {
							movies2.add(mov);

						}
					}
				}

			}
		}
		System.out.println(movies2.size() + " h");
		model.addAttribute("movies", movies2);
		String date = getCurrentLocalDateTimeStamp();
		model.addAttribute("date", date);
		return "odabirSadrzaja";
	}

	public String getCurrentLocalDateTimeStamp() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	@RequestMapping("/movieHandler")
	public String selectMovie(Model model, HttpSession session, @RequestParam("movie") String idMovie,
			@RequestParam("d1") String datum) throws IOException, ServletException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Cinema> cinemas = bioskopi.getCurrent();
		List<Movie> movies = movieService.getCurrent();
		List<Termin> termini = terminService.getCurrent();
		List<Termin> terminihelp = new ArrayList<Termin>();
		for (Termin t : termini) {
			System.out.println(t.getBioskop());
			if (t.getDatum().equals(datum)) {
				
				if (currentCinema.getId().equalsIgnoreCase(t.getBioskop().getId())) {
					if (idMovie.equalsIgnoreCase(t.getFilm().getId())) {
						session.setAttribute("termin", t);
						session.setAttribute("sala", t.getSala());
						session.setAttribute("mov", t.getFilm());
						Sediste[][] nov1 = t.getSala().getMesta();
						session.setAttribute("a", t.getSala().getMesta());
						session.setAttribute("a1", nov1.length);
						Sediste[][] nov = t.getSala().getMesta();
						session.setAttribute("b", nov[0].length);
						terminihelp.add(t);

					}
				}

			}
		}

		model.addAttribute("termini", terminihelp);

		return "seats";
	}

	@RequestMapping("/takeSeats")
	public String takeSeat(Model model, HttpSession session, @RequestParam("sediste") String sediste,
			@RequestParam("red") String red) throws IOException, ServletException {
		Sediste[][] sedista = (Sediste[][]) session.getAttribute("a");
		User user = (User) session.getAttribute("user");
		Sala sala = (Sala) session.getAttribute("sala");
		Termin termin = (Termin) session.getAttribute("termin");
		
		for(int i = 0; i < sedista.length; i++) {
			for(int j = 0; j < sedista[i].length;j++) {
				if(i == Integer.valueOf(red) && j == Integer.valueOf(sediste)) {
					sedista[i][j].setZauzeto(true);
					sedista[i][j].setSala(sala);
					sedista[i][j].setTermin(termin);
					sedista[i][j].setUser(user);
					session.setAttribute("sediste", sedista[i][j]);
					return "home";
				}
			}
		}
		return "home";
	}
}
