package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import model.Cinema;
import service.CinemaService;

@Controller
public class CinemaController {
	private CinemaService cinemaService;
	
	public CinemaService getCityService() {
		return cinemaService;
	}

	@Autowired
	public void setCinemaService(CinemaService cinemaService) {
		this.cinemaService = cinemaService;
	}

	@RequestMapping("/cinemas")
	public String showCinemas(Model model) {
		List<Cinema> cinemas = cinemaService.getCurrent();
		model.addAttribute("cinemas", cinemas);		
		return "cinemas";
	}
	
}
