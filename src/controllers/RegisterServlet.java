package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import model.User;
import service.UserService;

/**
 * Servlet implementation class RegisterServlet
 */
@MultipartConfig
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		String username = request.getParameter("uname");
		String password = request.getParameter("psw");
		String email = request.getParameter("email");
		String type = request.getParameter("type");
		String password1 = request.getParameter("psw1");
		String code = "";
		UserService userService = (UserService) session.getAttribute("userService");
		String appPath = request.getServletContext().getRealPath("");
		Part filePart = request.getPart("image");
		String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
		fileName = makePhoto(appPath, filePart, fileName);

		List<User> users = (List<User>) session.getAttribute("users");
		Boolean flag = false;

		for (User u : users) {
			if (u.getUsername().equalsIgnoreCase(username) || !(password.equalsIgnoreCase(password1))
					|| u.getEmail().equalsIgnoreCase(email)) {
				flag = true;
			}
		}

		if (flag) {
			session.setAttribute("isGood", false);
			RequestDispatcher disp = request.getRequestDispatcher("");
			disp.forward(request, response);
		} else {
			code = sendEmail(email);
			User user = new User(code, username, password, email, type, "false", fileName);
			session.setAttribute("user", user);
			System.out.println(user);
			userService.create(user);
			RequestDispatcher disp = request.getRequestDispatcher("");
			disp.forward(request, response);

		}
	}

	static String makePhoto(String appPath, Part filePart, String fileName) throws IOException {
		if (!fileName.equalsIgnoreCase("")) {

			filePart.write(
					"C:\\Users\\Pantelica\\eclipse-workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\wtpwebapps\\isaimrs\\resources\\"
							+ fileName);
		} else {
			fileName = "default-avatar.png";
		}
		return fileName;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private String generateCode() {
		RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder().withinRange('0', 'z')
				.filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS).build();
		String s = randomStringGenerator.generate(12);
		return s;
	}

	private String sendEmail(String email) {

		// Sender's email ID needs to be mentioned
		String from = "awesomedude996@gmail.com";
		String pass = "pantelaa23";
		// Recipient's email ID needs to be mentioned.
		String to = "milospantic96@hotmail.com";
		String code = generateCode();
		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();
		// Setup mail server
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.user", from);
		properties.put("mail.smtp.password", pass);
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(RecipientType.TO, new InternetAddress(to));

			message.setSubject("This is activation code. Use it to activate your account.");

			message.setText(code);

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("Sent message successfully....");
			return code;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return "";
	}

}
