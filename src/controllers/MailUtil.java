package controllers;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailUtil {
	private MailSender mailSender;

    public MailUtil(MailSender mailSender)
    {
        this.mailSender=mailSender;
    }
    public void sendResetPasswordMail(String email, String password) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("mymail@gmail.com");
        message.setTo(email);
        message.setSubject("Your new password!");
        message.setText("Here is your new account login password - " + password);
        mailSender.send(message);
    }
}
