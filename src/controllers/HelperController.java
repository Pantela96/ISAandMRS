package controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import model.Cinema;
import model.Movie;
import service.CinemaService;
import service.MovieService;
import service.UserService;

@Controller
public class HelperController {
	
	private UserService userService;
	private MovieService movieService;
	private CinemaService cinemaService;
	
	
	public MovieService getCinemaService() {
		return movieService;
	}

	@Autowired
	public void setCinemaService(CinemaService cinemaService) {
		this.cinemaService = cinemaService;
	}
	
	public MovieService getMovieService() {
		return movieService;
	}

	@Autowired
	public void setMovieService(MovieService movieService) {
		this.movieService = movieService;
	}
	
	
	public UserService getCityService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@RequestMapping("/help")
	public String getHelp(Model model) {
		List<Movie> movies = movieService.getCurrent();
		model.addAttribute("movies", movies);
		return "help";
	}
	
	@RequestMapping("/cinemaCheck")
	public String getCinema(Model model,@RequestParam("uname") String user,@RequestParam("psw") String pass,@RequestParam("remember")String remember) {
		List<Cinema> cinemas = cinemaService.getCurrent();
		model.addAttribute("cinemas", cinemas);
		return "help";
	}
	
}
