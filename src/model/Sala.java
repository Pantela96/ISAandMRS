package model;

import java.util.ArrayList;

public class Sala {

	String id;
	int redovi;
	int sedista;
	Sediste[][] mesta = new Sediste[redovi][sedista];

	public Sala(String id, int redovi, int sedista, Sediste[][] mesta) {
		super();
		this.id = id;
		this.redovi = redovi;
		this.sedista = sedista;
		this.mesta = mesta;
	}

	public  Sediste[][] fillMesta(int redovi, int sedista, Sediste[][] mesto) {
		for (int i = 0; i < redovi; i++) {
			for (int j = 0; j < sedista; j++) {
				mesto[i][j] = new Sediste("0", 0, 0, null, null, null, false);
			}
		}
		return mesto;
	}

	public Sediste[][] getMesta() {
		return mesta;
	}

	public void setMesta(Sediste[][] mesta) {
		this.mesta = mesta;
	}

	public Sala() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getRedovi() {
		return redovi;
	}

	public void setRedovi(int red) {
		this.redovi = red;
	}

	public int getSedista() {
		return sedista;
	}

	public void setSedista(int sedista) {
		this.sedista = sedista;
	}

}
