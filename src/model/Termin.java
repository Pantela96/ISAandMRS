package model;

import java.util.Date;

public class Termin {
	private String id;
	private Cinema bioskop;
	private Movie film;
	private String datum;
	private double cena;
	private Sala sala;
	private String vremeProjekcije;
	
	
	public String getVremeProjekcije() {
		return vremeProjekcije;
	}
	public void setVremeProjekcije(String vremeProjekcije) {
		this.vremeProjekcije = vremeProjekcije;
	}
	public Termin() {
		super();
	}
	public Termin(String id, Cinema bioskop, Movie film, String datum, double cena, Sala sala, String vP) {
		
		super();
		this.id = id;
		System.out.println(datum + " je1");
		this.bioskop = bioskop;
		this.film = film;
		this.datum = datum;
		this.cena = cena;
		this.sala = sala;
		this.vremeProjekcije = vP;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Cinema getBioskop() {
		return bioskop;
	}
	public void setBioskop(Cinema bioskop) {
		this.bioskop = bioskop;
	}
	public Movie getFilm() {
		return film;
	}
	public void setFilm(Movie film) {
		this.film = film;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String date) {
		this.datum = date;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}
	
}
