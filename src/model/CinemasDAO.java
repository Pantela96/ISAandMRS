package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import service.MovieService;

@Component("cinemasDAO")
public class CinemasDAO {
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		System.out.println("usao sam");
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	private MovieService movieService;

	public MovieService getMovieService() {
		return movieService;
	}

	@Autowired
	public void setMovieService(MovieService movieService) {
		this.movieService = movieService;
	}

	public List<Cinema> getCinemas() {
		return jdbc.query("select * from cinema", new RowMapper<Cinema>() {
			public Cinema mapRow(ResultSet rs, int rowNum) throws SQLException {
				ArrayList<String> movies = new ArrayList<String>();
				Cinema cinema = new Cinema();
				cinema.setId(rs.getString("id"));
				cinema.setAdress(rs.getString("location"));
				cinema.setName(rs.getString("name"));
				cinema.setSlika(rs.getString("slika"));
				System.out.println("prosao");
				cinema.setDescription(rs.getString("description"));
				String novi = rs.getString("movies");
				String[] list = novi.split(",");
				List<Movie> moviesReal = movieService.getCurrent();
				for(String s: list) {
					for(Movie m : moviesReal) {
						if(m.getId().equalsIgnoreCase(s)) {
							movies.add(s);
						}
					}
				}
				cinema.setMovies(movies);
				cinema.setSupport(movies);
				return cinema;
			}

		});
	}

	public boolean update(Cinema offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		
		return jdbc.update("update cinema set name =: name, location=:adress, description=:description, slika=:slika, movies=:support where id=:id",
				params) == 1;
	}

	public boolean create(Cinema offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		System.out.println("prosao");

		return jdbc.update(
				"insert into cinema (id,name, location, description,slika, movies) values (:id,:name, :adress, :description,:slika, :support)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Cinema> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into cinema (id, name, location, description,slika,movies) values (:id,:name, :adress, :description,:slika, :support)",
				params);
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from cinema where id=:id", params) == 1;
	}
}
