package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("moviesDAO")
public class MoviesDAO {
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		System.out.println("usao sam");
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}
	
	public List<Movie> getMovies() {
		return jdbc.query("select * from movies", new RowMapper<Movie>() {
			public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
				ArrayList<String> actors = new ArrayList<String>();
				Movie movie = new Movie();
				movie.setId(rs.getString("id"));
				movie.setName(rs.getString("name"));
				movie.setGenre(rs.getString("genre"));
				movie.setDirector(rs.getString("director"));
				movie.setLength(rs.getDouble("length"));
				movie.setSlika(rs.getString("slika"));
				movie.setRating(rs.getDouble("rating"));
				movie.setPrice(rs.getDouble("price"));
				String help = rs.getString("actors");
				String[] list = help.split(",");
				for(String a:list) {
					actors.add(a);
				}
				movie.setActors(actors);
				movie.setSupport(actors);
				return movie;
			}

		});
	}
	
	public boolean update(Movie offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		
		return jdbc.update("update movies set name=:name, genre=:genre, director=:director, length=:length, slika=:slika, rating=:rating, price=:price, actors =: support where id=:id", params) == 1;
	}
	
	public boolean create(Movie offer) {
		
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		
		return jdbc.update("insert into movies (id, name, genre, director,length, slika, rating, price, actors) values (:id, :name, :genre, :director, :length, :slika, :rating, :price. :support)", params) == 1;
	}
	
	@Transactional
	public int[] create(List<Movie> offers) {
		
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());
		
		return jdbc.batchUpdate("insert into movies (id, name, genre, director, length, slika, rating, price, actors) values (:id, :name, :genre, :director, :length, slika, :rating, :price, :support)", params);
	}
	
	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);
		
		return jdbc.update("delete from movies where id=:id", params) == 1;
	}
}
