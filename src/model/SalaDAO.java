package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("salaDAO")
public class SalaDAO {
	private NamedParameterJdbcTemplate jdbc;
	HttpSession session;

	@Autowired
	public void setDataSource(DataSource jdbc) {

		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Sala> getSala() {
		return jdbc.query("select * from sala", new RowMapper<Sala>() {
			public Sala mapRow(ResultSet rs, int rowNum) throws SQLException {
				Sala message = new Sala();
				message.setId(rs.getString("id"));
				message.setRedovi(rs.getInt("redovi"));
				message.setSedista(rs.getInt("sedista"));
				Sediste[][] help = new Sediste[message.getRedovi()][message.getSedista()];
				Sediste[][] help2 = new Sediste[message.getRedovi()][message.getSedista()];
				help2 = message.fillMesta(message.getRedovi(), message.getSedista(),help);
				message.setMesta(help2);
				return message;
			}

		});
	}

	public boolean update(Sala offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update("update sala set redovi:= redovi, sedista =: sedista where id=:id",
				params) == 1;
	}

	public boolean create(Sala offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update(
				"insert into sala (id,redovi, sedista) values (:id, :redovi, :sedista)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Sala> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into sala (id, redovi, sedista) values (:id, :redovi, :sedista)",
				params);
	}

	public boolean delete(String id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from sala where id=:id", params) == 1;
	}
}
