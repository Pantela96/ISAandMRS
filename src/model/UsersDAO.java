package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component("usersDao")
public class UsersDAO {
		private NamedParameterJdbcTemplate jdbc;

		@Autowired
		public void setDataSource(DataSource jdbc) {
			System.out.println("usao sam");
			this.jdbc = new NamedParameterJdbcTemplate(jdbc);
		}
		
		public List<User> getUsers() {
			return jdbc.query("select * from user", new RowMapper<User>() {
				public User mapRow(ResultSet rs, int rowNum) throws SQLException {
					User user = new User();
					user.setId(rs.getString("id"));
					user.setUsername(rs.getString("name"));
					user.setPassword(rs.getString("password"));
					user.setEmail(rs.getString("email"));
					user.setTip(rs.getString("tip"));
					user.setActive(rs.getString("active"));
					user.setSlika(rs.getString("slika"));
					return user;
				}

			});
		}
		
		public boolean update(User offer) {
			BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
			
			return jdbc.update("update user	 set name=:username, password=:password, email=:email, tip=:tip, active=:active, slika=:slika where id=:id", params) == 1;
		}
		
		public boolean create(User offer) {
			
			BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
			
			return jdbc.update("insert into user (id, name, email, password,tip,active,slika) values (:id, :username, :email, :password, :tip,:active, :slika)", params) == 1;
		}
		
		@Transactional
		public int[] create(List<User> offers) {
			
			SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());
			
			return jdbc.batchUpdate("insert into user (id, name, password, email, active,slika) values (:id, :username, :password, :email, :active, :slika)", params);
		}
		
		public boolean delete(int id) {
			MapSqlParameterSource params = new MapSqlParameterSource("id", id);
			
			return jdbc.update("delete from offers where id=:id", params) == 1;
		}
}
