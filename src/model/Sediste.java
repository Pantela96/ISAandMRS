package model;

public class Sediste {
	
	String id;
	int mesto;
	int red;
	User user;
	Termin termin;
	Sala sala;
	boolean zauzeto = false;
	
	
	
	public Sediste(String id, int red, int mesto, User user, Termin termin, Sala sala, boolean zauzeto) {
		super();
		this.id = id;
		this.red = red;
		this.mesto = mesto;
		this.user = user;
		this.termin = termin;
		this.sala = sala;
		this.zauzeto = zauzeto;
	}
	public Sediste() {
		super();
	}

	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getRed() {
		return red;
	}
	public void setRed(int red) {
		this.red = red;
	}
	public int getMesto() {
		return mesto;
	}
	public void setMesto(int mesto) {
		this.mesto = mesto;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Termin getTermin() {
		return termin;
	}
	public void setTermin(Termin termin) {
		this.termin = termin;
	}
	public boolean isZauzeto() {
		return zauzeto;
	}
	public void setZauzeto(boolean zauzeto) {
		this.zauzeto = zauzeto;
	}

}
