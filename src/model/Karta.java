package model;

public class Karta {
	String terminId;
	String sediste;
	
	
	public Karta(String terminId, String sediste) {
		super();
		this.terminId = terminId;
		this.sediste = sediste;
	}
	public Karta() {
		super();
	}
	public String getTerminId() {
		return terminId;
	}
	public void setTerminId(String terminId) {
		this.terminId = terminId;
	}
	public String getSediste() {
		return sediste;
	}
	public void setSediste(String sediste) {
		this.sediste = sediste;
	}
	
}
