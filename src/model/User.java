package model;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class User implements Comparable<User> {
	String id;
	String username;
	String password;
	String email;
	String tip;
	String active;
	String slika;
	Termin termin;

	SortedSet<User> sendedRequests = new TreeSet<User>();
	SortedSet<User> receivedRequests = new TreeSet<User>();
	SortedSet<User> friendList = new TreeSet<User>();
	SortedSet<Karta> cardList = new TreeSet<Karta>();

	public Termin getTermin() {
		return termin;
	}

	public void setTermin(Termin termin) {
		this.termin = termin;
	}

	public SortedSet<Karta> getCardList() {
		return cardList;
	}

	public void setCardList(SortedSet<Karta> cardList) {
		this.cardList = cardList;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public Set<User> getFriendList() {
		return friendList;
	}

	public void setFriendList(SortedSet<User> friendList) {
		this.friendList = friendList;
	}

	public Set<User> getSendedRequests() {
		return sendedRequests;
	}

	public void setSendedRequests(SortedSet<User> sendedRequests) {
		this.sendedRequests = sendedRequests;
	}

	public Set<User> getReceivedRequests() {
		return receivedRequests;
	}

	public void setReceivedRequests(SortedSet<User> receivedRequests) {
		this.receivedRequests = receivedRequests;
	}

	public User(String id, String username, String password, String email, String tip, String active, String slika) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.active = active;
		this.tip = tip;
		this.slika = slika;
		sendedRequests = new TreeSet<User>();
		receivedRequests = new TreeSet<User>();
		friendList = new TreeSet<User>();
		SortedSet<Karta> cardList = new TreeSet<Karta>();
	}

	public String getTip() {
		return tip;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public User() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int compareTo(User o2) {
		// TODO Auto-generated method stub
		String x1 = this.getUsername();
		String x2 = ((User) o2).getUsername();
		int sComp = x1.compareTo(x2);

		return sComp;
	}



}
