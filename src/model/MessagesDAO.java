package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("messagesDAO")
public class MessagesDAO {
	private NamedParameterJdbcTemplate jdbc;
	HttpSession session;

	@Autowired
	public void setDataSource(DataSource jdbc) {

		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Message> getMessages() {
		return jdbc.query("select * from message", new RowMapper<Message>() {
			public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
				Message message = new Message();
				message.setId(rs.getString("id"));
				message.setSender(rs.getString("sender"));
				message.setReceiver(rs.getString("receiver"));
				return message;
			}

		});
	}

	public boolean update(Message offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update("update message set sender, receiver =: receiver where id=:id",
				params) == 1;
	}

	public boolean create(Message offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update(
				"insert into message (id,sender, receiver) values (:id, :sender, :receiver)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Message> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into message (id, sender, receiver) values (:id, :sender, :receiver)",
				params);
	}

	public boolean delete(String id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from message where id=:id", params) == 1;
	}

	
}
