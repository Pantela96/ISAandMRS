package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import service.CinemaService;
import service.MovieService;
import service.SalaService;
import service.TerminService;

@Component("terminDAO")
public class TerminDAO {
	private NamedParameterJdbcTemplate jdbc;
	private CinemaService bioskopi;
	private TerminService terminService;
	private MovieService movieService;
	private SalaService salaService;
	



	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public SalaService getSalaService() {
		return salaService;
	}

	@Autowired
	public void setSalaService(SalaService salaService) {
		this.salaService = salaService;
	}

	public TerminService getTerminService() {
		return terminService;
	}

	@Autowired
	public void setTerminService(TerminService terminService) {
		this.terminService = terminService;
	}
	
	
	public CinemaService getCinemaService() {
		return bioskopi;
	}

	@Autowired
	public void setCinemaService(CinemaService cinemaService) {
		this.bioskopi = cinemaService;
	}
	
	public MovieService getMovieService() {
		return movieService;
	}

	@Autowired
	public void setMovieService(MovieService movieService) {
		this.movieService = movieService;
	}
	
	
	public List<Termin> getTermin() {
		return jdbc.query("select * from termin", new RowMapper<Termin>() {
			public Termin mapRow(ResultSet rs, int rowNum) throws SQLException {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				List<Movie> movies = movieService.getCurrent();
				List<Cinema> cinemas = bioskopi.getCurrent();
				List<Sala> sale = salaService.getCurrent();
				Termin cinema = new Termin();
				cinema.setId(rs.getString("id"));
				for(Cinema c: cinemas) {
					if(c.getId().equalsIgnoreCase(rs.getString("bioskop"))) {
						cinema.setBioskop(c);
					}
				}
				for(Movie c: movies) {
					if(c.getId().equalsIgnoreCase(rs.getString("film"))) {
						cinema.setFilm(c);
					}
				}

				cinema.setDatum(rs.getString("datum"));
				cinema.setVremeProjekcije(rs.getString("vremeProjekcije"));
				cinema.setCena(Double.parseDouble(rs.getString("cena")));
				for(Sala a:sale) {
					if(a.getId().equalsIgnoreCase(rs.getString("sala"))) {
						cinema.setSala(a);
					}
				}

				return cinema;
			}

		});
	}

	public boolean update(Cinema offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		
		return jdbc.update("update termin set bioskop =: bioskop.name, film=:film.name, datum=:datum, cena=:cena, sala=:sala, vremeProjekcije=:vremeProjekcije where id=:id",
				params) == 1;
	}

	public boolean create(Termin offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		System.out.println("prosao");

		return jdbc.update(
				"insert into termin (id,bioskop, film, description,sala,cena,vremeProjekcije) values (:id,:bioskop.name, :film.name, :datum,:sala,:cena,:vremeProjekcije)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Termin> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into termin (id,bioskop, film, description,sala,cena,vremeProjekcije) values (:id,:bioskop.name, :film.name, :datum,:sala,:cena,:vremeProjekcije)",
				params);
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from termin where id=:id", params) == 1;
	}
}
