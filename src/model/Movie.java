package model;

import java.util.ArrayList;

public class Movie {
	String id;
	String name;
	ArrayList<String> actors;
	String genre;
	String director;
	double length;
	String slika = "default-avatar.png"; //za sad
	double rating;
	String description;
	String support;
	

	public Movie() {
		super();
	}
	double price;
	
	
	public String getSupport() {
		return support;
	}
	public void setSupport(ArrayList<String> actors)  {
		String movieString = "";
		for(String m: actors) {
			if(movieString.equalsIgnoreCase("")) {
				movieString = m;
				System.out.println(movieString);
			}else {
				movieString += "," + m;
			}
				
		}
		System.out.println(movieString);
		this.support = movieString;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Movie(String id, String name, ArrayList<String> actors, String genre, String director, double length, String slika,
			double rating, String description, double price) {
		super();
		this.id = id;
		this.name = name;
		this.actors = actors;
		this.genre = genre;
		this.director = director;
		this.length = length;
		this.slika = slika;
		this.rating = rating;
		this.description = description;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getActors() {
		return actors;
	}
	public void setActors(ArrayList<String> actors) {
		this.actors = actors;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
