package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import service.MovieService;
import service.TheaterService;


@Component("theaterDAO")
public class TheaterDAO {
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}


	private MovieService movieService;

	public MovieService getMovieService() {
		return movieService;
	}

	@Autowired
	public void setMovieService(MovieService movieService) {
		this.movieService = movieService;
	}

	public List<Theater> getTheaters() {
		return jdbc.query("select * from theater", new RowMapper<Theater>() {
			public Theater mapRow(ResultSet rs, int rowNum) throws SQLException {
				System.out.println("usao123");
				ArrayList<String> movies = new ArrayList<String>();
				Theater cinema = new Theater();
				cinema.setId(rs.getString("id"));
				cinema.setAdress(rs.getString("location"));
				cinema.setName(rs.getString("name"));
				cinema.setSlika(rs.getString("slika"));
				cinema.setDescription(rs.getString("description"));
				String novi = rs.getString("movies");
				String[] list = novi.split(",");
				System.out.println(cinema + "1");
				List<Movie> moviesReal = movieService.getCurrent();
				System.out.println(cinema + "2");
				for(String s: list) {
					for(Movie m : moviesReal) {
						if(m.getId().equalsIgnoreCase(s)) {
							movies.add(s);
						}
					}
				}
				cinema.setMovies(movies);
				cinema.setSupport(movies);
				System.out.println(cinema);
				return cinema;
			}

		});
	}

	public boolean update(Theater offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);
		
		return jdbc.update("update theater set name =: name, location=:adress, description=:description, slika=:slika, movies=:support where id=:id",
				params) == 1;
	}

	public boolean create(Theater offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update(
				"insert into theater (id,name, location, description,slika, movies) values (:id,:name, :adress, :description,:slika, :support)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Theater> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into theater (id, name, location, description,slika,movies) values (:id,:name, :adress, :description,:slika, :support)",
				params);
	}

	public boolean delete(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from theater where id=:id", params) == 1;
	}
}
