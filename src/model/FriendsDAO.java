package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("friendsDAO")
public class FriendsDAO {
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<Friend> getFriends() {
		return jdbc.query("select * from friends", new RowMapper<Friend>() {
			public Friend mapRow(ResultSet rs, int rowNum) throws SQLException {
				Friend friend = new Friend();
				friend.setId(rs.getString("id"));
				friend.setOrigin(rs.getString("origin"));
				friend.setOther(rs.getString("other"));
				return friend;
			}

		});
	}

	public boolean update(Friend offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update("update friends set origin=:origin, other=:other where id=:id",
				params) == 1;
	}

	public boolean create(Friend offer) {

		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update(
				"insert into friends (id, origin, other) values (:id, :origin, :other)",
				params) == 1;
	}

	@Transactional
	public int[] create(List<Friend> offers) {

		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(offers.toArray());

		return jdbc.batchUpdate("insert into friends (id, origin, other) values (:id, :origin, :other)",
				params);
	}

	public boolean delete(String id) {
		System.out.println(id + "id je");
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);
		
		return jdbc.update("delete from friends where id=:id", params) == 1;
	}
}
