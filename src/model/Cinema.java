package model;

import java.util.ArrayList;

public class Cinema {
	String id;
	String name;
	String adress;
	String description;
	ArrayList<String> movies;
	String support;
	String slika;
	//treba da se ubace karte i neka konfiguracija
	
	
	public String getSupport() {
		return support;
	}
	public void setSupport(ArrayList<String> movies) {
		String movieString = "";
		for(String m: movies) {
			if(movieString.equalsIgnoreCase("")) {
				movieString = m;
				System.out.println(movieString);
			}else {
				movieString += "," + m;
			}
				
		}
		System.out.println(movieString);
		this.support = movieString;
	}
	public Cinema(String id,String name, String adress, String description,  String support, String slika) {
		super();
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.description = description;
		this.support = support;
		this.slika = slika;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<String> getMovies() {
		return movies;
	}
	public void setMovies(ArrayList<String> movies) {
		this.movies = movies;
	}
	public Cinema() {
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ArrayList<String> getRepertoire() {
		return movies;
	}
	public void setRepertoire(ArrayList<String> repertoire) {
		this.movies = repertoire;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	

}
