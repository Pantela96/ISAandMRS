package model;

public class Friend {
	String id;
	String origin;
	String other;
	
	public Friend() {
		super();
	}
	public Friend(String id, String origin, String other) {
		super();
		this.id = id;
		this.origin = origin;
		this.other = other;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
}
