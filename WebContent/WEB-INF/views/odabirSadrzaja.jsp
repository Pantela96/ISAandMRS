<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${pageContext.request.contextPath}/resources/bootstrap.css"
	rel="stylesheet" type="text/css">


<title>App</title>
<style type="text/css">
/* Some custom styles to beautify this example */
.demo-content {
	padding: 15px;
	font-size: 18px;
	min-height: 150px;
	max-height: 150px;
	background: #fff;
}

.hr {
	display: block;
	border: 0;
	border-top: 1px solid #ccc;
}
</style>

</head>
<body>
	<c:choose>
		<c:when test="${user.username == 'x'}">
			<a href="${pageContext.request.contextPath}/" class="btn btn-info">Back</a>
		</c:when>
		<c:when test="${user.username != 'x'}">
			<form action="${pageContext.request.contextPath}/check"
				, method="post">
				<input type="hidden" value="${user.username}" name="uname" /> <input
					type="hidden" value="${user.password}" name="psw" /> <input
					type="submit" value="Back" class="btn btn-info">
			</form>
		</c:when>
	</c:choose>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 container">
				<h3>Pregled Bioskopa</h3>
			</div>
		</div>
		<hr />
		<c:forEach var="cinema" items="${movies}">
			<div class="row">
				<div class="col-md-3 demo-content">
					<img
						src="${pageContext.request.contextPath}/resources/${cinema.slika}"
						style="max-width: 200px; max-height: 200px">
				</div>
				<div class="col-md-6 demo-content">
					<h5>Ime filma:${cinema.name}</h5>
					<p>Zanr:${cinema.genre}</p>
					<p>Rejting:${cinema.rating}</p>
					<p>Cena: ${cinema.price}</p>

				</div>
				<div class="col-md-3 demo-content">

					<form action="${pageContext.request.contextPath}/movieHandler">
						<input type="hidden" value="${cinema.id}" name="movie">
						<div class="lol">
							<input type="date" id='d1' name='d1' min="${date}" value="${date}">
						</div>
						<input type="submit" value="Projekcije" class="btn btn-info"
							style="position: relative;" method="post" />

					</form>


					<hr />
					<a href="#" class="btn btn-primary"
						style="position: relative; top: -10px;">Repertoar-Nedostupno</a>
				</div>
			</div>
			<hr />
		</c:forEach>

	</div>

	<script
		��src="${pageContext.request.contextPath}/resources/jquery-3.3.1.js"></script>
	<script
		��src="${pageContext.request.contextPath}/resources/bootstrap.js"></script>

	<script>
		var d1 = new Date();
		var y1 = d1.getFullYear();
		var m1 = d1.getMonth() + 1;
		if (m1 < 10) {
			m1 = "0" + m1
		};
		var dt1 = d1.getDate();
		if (dt1 < 10) {
			dt1 = "0" + dt1
		};
		var d2 = y1 + "-" + m1 + "-" + dt1;

		var future = new Date();
		future.setDate(future.getDate() + 5);
		var futureYear = future.getFullYear();
		var futureMonth = future.getMonth() + 1;
		if (futureMonth < 10) {
			futureMonth = "0" + futureMonth
		};
		var futureDay = future.getDate();
		if (futureDay < 10) {
			futureDay = "0" + futureDay
		};
		var futureDate = futureYear + "-" + futureMonth + "-" + futureDay;
		var els = document.querySelectorAll('.lol');
		console.log(els.length);
		for (var i = 0; i < els.length; i++) {
			console.log(els[i].value);
			els[i].setAttribute("value", d2);
			els[i].setAttribute("min", futureDate);
		}
	</script>

</body>
</html>