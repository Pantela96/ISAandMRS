<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${pageContext.request.contextPath}/resources/bootstrap.css"
	rel="stylesheet" type="text/css">


<title>Lista Objekata</title>
<style type="text/css">
/* Some custom styles to beautify this example */
.demo-content {
	padding: 15px;
	font-size: 18px;
	min-height: 150px;
	max-height: 150px;
	background: #fff;
}

.hr {
	display: block;
	border: 0;
	border-top: 1px solid #ccc;
}
</style>

</head>
<body>
	<c:choose>
		<c:when test="${user.username == 'x'}">
			<a href="${pageContext.request.contextPath}/" class="btn btn-info">Back</a>
		</c:when>
		<c:when test="${user.username != 'x'}">
			<form action="${pageContext.request.contextPath}/check"
				, method="post">
				<input type="hidden" value="${user.username}" name="uname" /> <input
					type="hidden" value="${user.password}" name="psw" /> <input
					type="submit" value="Back" class="btn btn-info">
			</form>
		</c:when>
	</c:choose>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 container">
				<h3>Pregled Bioskopa</h3>
			</div>
		</div>
		<hr />
		<c:forEach var="cinema" items="${cinemas}">
			<div class="row">
				<div class="col-md-3 demo-content">
					<img
						src="${pageContext.request.contextPath}/resources/${cinema.slika}"
						style="max-width: 200px; max-height: 200px">
				</div>
				<div class="col-md-6 demo-content">
					<h5>${cinema.name}</h5>
					<p>${cinema.adress}</p>
					<p>${cinema.description}</p>
				</div>
				<div class="col-md-3 demo-content">
					<form action="${pageContext.request.contextPath}/rezervacija">
						<input type="hidden" value="${cinema.id}" name="ustanova"> <input
							type="submit" value="Rezervacije" class="btn btn-info"
							style="position: relative;" method = "post" />
					</form>
					<hr />
					<a href="#" class="btn btn-primary">Repertoar-Nedostupno</a>
				</div>
			</div>
			<hr />
		</c:forEach>

		<c:forEach var="cinema" items="${theaters}">
			<div class="row">
				<div class="col-md-3 demo-content">
					<img
						src="${pageContext.request.contextPath}/resources/${cinema.slika}"
						style="max-width: 200px; max-height: 200px">
				</div>
				<div class="col-md-6 demo-content">
					<h5>${cinema.name}</h5>
					<p>${cinema.adress}</p>
					<p>${cinema.description}</p>
				</div>
				<div class="col-md-3 demo-content">
					<form action="${pageContext.request.contextPath}/rezervacija">
						<input type="hidden" value="${cinema.id}" name="ustanova"> <input
							type="submit" value="Rezervacije" class="btn btn-info"
							style="position: relative;" method = "post" />
					</form>
					<hr />
					<a href="#" class="btn btn-primary">Repertoar-Nedostupno</a>
				</div>
			</div>
			<hr />
		</c:forEach>

	</div>

	<script
		��src="${pageContext.request.contextPath}/resources/jquery-3.3.1.js"></script>
	<script
		��src="${pageContext.request.contextPath}/resources/bootstrap.js"></script>

</body>
</html>