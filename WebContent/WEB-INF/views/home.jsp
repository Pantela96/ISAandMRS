<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link
	href="${pageContext.request.contextPath}/resources/stilovi.css?v=1.1"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/bootstrap.css?v=1.1"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/login.css?v=1.1"
	rel="stylesheet">
<script type=”text/javascript”
	src=”bootstrap/js/bootstrap.min.js”></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>

</head>
<body style="background: #A4A4A4;)">





	<c:if test="${user.username != 'x'}">
		<form action="/isaimrs/logoutHandle" , method="post">
			<input type="submit" class="btn btn-info" value="Logout"
				style="position: absolute; left: 95%; top: 0.75%;">
		</form>
	</c:if>
	
	<jsp:include page="profilePhoto.jsp" />
	
	<jsp:include page="login.jsp" />

	<jsp:include page="register.jsp" />

	<jsp:include page="requests.jsp" />

	<jsp:include page="friendList.jsp" />

	<jsp:include page="addFriend.jsp" />

	<jsp:include page="activation.jsp" />





	<c:if test="${user.username != 'x'}">
		<c:if test="${user.active != false}">
			<form action="${pageContext.request.contextPath}/cinemas"
				method="post">
				<button type="submit"
					action="${pageContext.request.contextPath}/cinemas"
					class="btn btn-success"
					style="position: relative; left: 61%; top: 0%;">Prikazi
					Bioskope</button>
			</form>
		</c:if>
		<c:if test="${user.active == false}">
			<form action="${pageContext.request.contextPath}/cinemas"
				method="post">
				<button type="submit"
					action="${pageContext.request.contextPath}/cinemas"
					class="btn btn-success"
					style="position: relative; left: 500%; top: 0%;">Prikazi
					Bioskope</button>
			</form>
		</c:if>
	</c:if>

	<c:if test="${user.username == 'x'}">
		<form action="${pageContext.request.contextPath}/cinemas"
			method="post">
			<button type="submit"
				action="${pageContext.request.contextPath}/cinemas"
				class="btn btn-success"
				style="position: relative; left: 15%; top: 0%;">Prikazi
				Bioskope</button>
		</form>
	</c:if>
	<c:if test="${user.active != false}">
		<jsp:include page="profile.jsp" />
	</c:if>
	<jsp:include page="changePassword.jsp" />
	<c:if test="${user.username != 'x'}">
		<c:if test="${user.active != false}">
			<form action="${pageContext.request.contextPath}/pozorista"
				method="post">
				<button type="submit"
					action="${pageContext.request.contextPath}/pozorista"
					class="btn btn-default"
					style="position: relative; left: -235%; top: 0%;">Prikazi
					Pozorista</button>
			</form>
		</c:if>
		<c:if test="${user.active == false}">
			<form action="${pageContext.request.contextPath}/pozorista"
				method="post">
				<button type="submit"
					action="${pageContext.request.contextPath}/pozorista"
					class="btn btn-default"
					style="position: relative; left: 290%; top: 0%;">Prikazi
					Pozorista</button>
			</form>
		</c:if>
	</c:if>
	<c:if test="${user.username == 'x'}">
		<form action="${pageContext.request.contextPath}/pozorista"
			method="post">
			<button type="submit"
				action="${pageContext.request.contextPath}/pozorista"
				class="btn btn-default"
				style="position: relative; left: 15%; top: 0%;">Prikazi
				Pozorista</button>
		</form>

	</c:if>
	<jsp:include page="reservation.jsp" />
	
</body>
</html>