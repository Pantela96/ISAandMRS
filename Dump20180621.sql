-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cinema`
--

DROP TABLE IF EXISTS `cinema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cinema` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `movies` longtext NOT NULL,
  `slika` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cinema`
--

LOCK TABLES `cinema` WRITE;
/*!40000 ALTER TABLE `cinema` DISABLE KEYS */;
INSERT INTO `cinema` VALUES (2,'Cinaplex Big','Beograd','Neki drugi opis','1,2','CineplBigBg.jpg'),(7,'Cinaplex Arena','Novi Sad','Neki opis','1,2,3','CinaplArenaNs.jpg'),(8,'Cineplex Delta','Beograd','Neki treci opis','2,3','CineplDeltaBg.jpg'),(9,'Cinaplex Nis','Nis','Neki cetvrti opis','1,3','CineplNis.jpg'),(10,'Cineplex Plaza','Kragujevac','Neki peti opis','1','CineplPlazaKg.jpg'),(11,'Cineplex Usce','Beograd','Neki seti opis','1,2,3','CineplUsceBg.jpg'),(12,'Cinestar Pancevo','Pancevo','Neki sedmi opis','1,3','CinesPan.jpg'),(13,'Cinestar NS','Novi Sad','Neki osmi opis','2,3','CinesNs.jpg');
/*!40000 ALTER TABLE `cinema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `origin` varchar(45) NOT NULL,
  `other` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (7,'yuMUSySi79Sq','20'),(8,'20','yuMUSySi79Sq'),(9,'21','lcS06oOtANrz'),(10,'lcS06oOtANrz','21');
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `sender` varchar(45) NOT NULL,
  `receiver` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (2,'1','20'),(3,'1','20'),(8,'21','51');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `actors` longtext,
  `genre` varchar(45) NOT NULL,
  `director` varchar(45) NOT NULL,
  `length` decimal(20,0) NOT NULL,
  `slika` longtext,
  `rating` decimal(20,0) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `price` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,'m','a,a,a','a','a',20,'brand_cigarettes_lucky_strike_red_8596_1600x900.jpg',20,'a',20),(2,'n','a','g','d',20,'brand_cigarettes_lucky_strike_red_8596_1600x900.jpg',5,'d',500);
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sala`
--

DROP TABLE IF EXISTS `sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sala` (
  `id` int(11) NOT NULL,
  `redovi` int(11) NOT NULL,
  `sedista` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`redovi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sala`
--

LOCK TABLES `sala` WRITE;
/*!40000 ALTER TABLE `sala` DISABLE KEYS */;
INSERT INTO `sala` VALUES (1,10,20),(2,10,10);
/*!40000 ALTER TABLE `sala` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sediste`
--

DROP TABLE IF EXISTS `sediste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sediste` (
  `id` int(11) NOT NULL,
  `red` int(11) DEFAULT NULL,
  `mesto` int(11) DEFAULT NULL,
  `userId` varchar(45) DEFAULT NULL,
  `terminId` varchar(45) DEFAULT NULL,
  `salaId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sediste`
--

LOCK TABLES `sediste` WRITE;
/*!40000 ALTER TABLE `sediste` DISABLE KEYS */;
INSERT INTO `sediste` VALUES (1,5,7,'21','1','1'),(2,5,7,'21','2','2');
/*!40000 ALTER TABLE `sediste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `termin`
--

DROP TABLE IF EXISTS `termin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `termin` (
  `id` int(11) NOT NULL,
  `bioskop` varchar(45) DEFAULT NULL,
  `film` varchar(45) DEFAULT NULL,
  `datum` varchar(45) DEFAULT NULL,
  `cena` double DEFAULT NULL,
  `sala` varchar(45) DEFAULT NULL,
  `vremeProjekcije` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `termin`
--

LOCK TABLES `termin` WRITE;
/*!40000 ALTER TABLE `termin` DISABLE KEYS */;
INSERT INTO `termin` VALUES (1,'2','1','2018-06-24',500,'1','19:45'),(2,'7','2','2018-06-24',500,'2','17:45');
/*!40000 ALTER TABLE `termin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theater`
--

DROP TABLE IF EXISTS `theater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `theater` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `movies` longtext NOT NULL,
  `slika` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theater`
--

LOCK TABLES `theater` WRITE;
/*!40000 ALTER TABLE `theater` DISABLE KEYS */;
INSERT INTO `theater` VALUES (2,'Cinaplex Big','Beograd','Neki drugi opis','1,2','CineplBigBg.jpg'),(7,'Cinaplex Arena','Novi Sad','Neki opis','1,2,3','CinaplArenaNs.jpg'),(8,'Cineplex Delta','Beograd','Neki treci opis','2,3','CineplDeltaBg.jpg'),(9,'Cinaplex Nis','Nis','Neki cetvrti opis','1,3','CineplNis.jpg'),(10,'Cineplex Plaza','Kragujevac','Neki peti opis','1','CineplPlazaKg.jpg'),(11,'Cineplex Usce','Beograd','Neki seti opis','1,2,3','CineplUsceBg.jpg'),(12,'Cinestar Pancevo','Pancevo','Neki sedmi opis','1,3','CinesPan.jpg'),(13,'Cinestar NS','Novi Sad','Neki osmi opis','2,3','CinesNs.jpg');
/*!40000 ALTER TABLE `theater` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` varchar(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `tip` varchar(20) DEFAULT NULL,
  `active` varchar(10) NOT NULL DEFAULT 'false',
  `slika` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('0DUZ1R3uKlde','mm5','mm5','mm5','User','false','brand_cigarettes_lucky_strike_red_8596_1600x900.jpg'),('10','mihajlo','mihajlo@gmail.com','12345','admin','false','default-avatar.png'),('101','novi3','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('11','marko96','marko@gmail.com','12345','User','false','default-avatar.png'),('20','','jovan@gmail.com','12345','user','true','default-avatar.png'),('21','ivan','ivan@gmail.com','12345','User','true','alpe_di_siusi_italy_nature_mountains_dolomites_94940_1600x900.jpg'),('29','samir2','damirhadzic996+1@gmail.com','12345','User','false','default-avatar.png'),('30','stevan','stevan@gmail.com','12345','user','false','default-avatar.png'),('31','a','a','123','User','false','default-avatar.png'),('41','b','b','b','User','false','default-avatar.png'),('51','milos96','milos96','12345','User','false','default-avatar.png'),('54sgsyQHL00J','b1','b11','b1','User','false','brand_cigarettes_lucky_strike_red_8596_1600x900.jpg'),('61','jovana','jovana','12345','User','false','default-avatar.png'),('71','novi','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('7gUDJi8F7ewJ','milos3','milospantic96+2@hotmail.com','12345','User','true','default-avatar.png'),('7VUfaSit7uB9','samir1','damirhadzic996@gmail.com','12345','User','false','default-avatar.png'),('81','novi1','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('91','novi2','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('91.1','n5','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('91.19999999999999','n7','aw','12345','User','false','default-avatar.png'),('91.29999999999998','n8','awesomedude996@gmail.com','12345','User','false','default-avatar.png'),('91.39999999999998','n10','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('91.49999999999997','n11','awesomedude996@gmail.com','12345','User','false','default-avatar.png'),('91.59999999999997','n12','milospantic96@hotmail.com','12345','User','false','default-avatar.png'),('91.69999999999996','n15','milospantic96+123@hotmail.com','12345','User','false','default-avatar.png'),('9bqeFjODCT4y','mm','mm','mm','User','true','default-avatar.png'),('a8UahMG0YN2Z','mm4','mm4','mm4','User','false','brand_cigarettes_lucky_strike_red_8596_1600x900.jpg'),('AI5tCcAhobu6','debil1','milospantic96+1122@hotmail.com','12345','User','true','default-avatar.png'),('BxtIdOkjiKyG','oguazz','ogauzz1@gmail.com','12345','User','false','default-avatar.png'),('Ga8XC56sibRX','k','k','k','User','false','lion_black_lion_mane_rock_56883_1600x900.jpg'),('JXVGTbyNzx3o','a1','1','1','User','false','default-avatar.png'),('lcS06oOtANrz','milos','milospantic96+1@hotmail.com','12345','User','true','prague_czech_republic_winter_buildings_people_59243_1600x900.jpg'),('mkb5CBmNj5hV','mmm','mmm','mmm','User','false','default-avatar.png'),('Qx1rCaNI4haw','debil','milospantic96+1111111@hotmail.com','12345','User','true','default-avatar.png'),('reRp58joUjBM','mm3','mm3','mm3','User','false','brand_cigarettes_lucky_strike_red_8596_1600x900.jpg'),('RtPTWOKjSbvf','mm1','milospantic96+98@hotmail.com','12345','User','false','IDP20170823_05669_001.jpg'),('yuMUSySi79Sq','milos','milospantic96+1@hotmail.com','12345','User','true','default-avatar.png'),('Zf61g4Xfy5hU','mm4','mm4','mm4','User','false','brand_cigarettes_lucky_strike_red_8596_1600x900.jpg'),('zxP8ObncKdwX','mm2','mm22','12345','User','false','IDP20170823_05669_001.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-21 16:04:12
